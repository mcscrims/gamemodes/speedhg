# SpeedHG



## Getting started

This is the game mode SpeedHG on the server McScrims.club. We want to bring SoupPvP to a new level with this game mode. Our goal with this game mode was to make it much easier for new players to enter the world of soup by adding a level system that gives players with a lower level starting items.

## License
[![MIT License](https://img.shields.io/gitlab/license/mcscrims/gamemodes/speedhg?&logo=github)](License)

All patches are licensed under the MIT license, unless otherwise noted in the patch headers.
